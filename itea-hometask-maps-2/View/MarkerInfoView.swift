//
//  MarkerInfoView.swift
//  itea-hometask-maps-2
//
//  Created by Валентин Петруля on 7/1/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {

    @IBOutlet weak var contentInfoView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
