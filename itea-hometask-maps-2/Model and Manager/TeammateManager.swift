//
//  TeammateManager.swift
//  itea-hometask-maps-2
//
//  Created by Валентин Петруля on 7/1/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class TeammateManager {
    let teammate1 = Teammate(avatar: "1", name: "Анатолий", surname: "Гордиенко", address: "ул. Приозерная, 2А", phone: "0939817010", latitude: 50.492714, longitude: 30.5183696)
    let teammate2 = Teammate(avatar: "2", name: "Вячеслав", surname: "Савицкий", address: "пр. Лобановского, 6А", phone: "0951853085", latitude: 50.4225673, longitude: 30.4541303)
    let teammate3 = Teammate(avatar: "3", name: "Ярослав", surname: "Павлюк", address: "бульвар Вацлава Гавела, 9А", phone: "0505584120", latitude: 50.4483754, longitude: 30.4183809)
    let teammate4 = Teammate(avatar: "4", name: "Алексей", surname: "Марфутин", address: "ул. Соловцова", phone: "0666817073", latitude: 50.4052317, longitude: 30.4873222)
    let teammate5 = Teammate(avatar: "5", name: "Лилия", surname: "Несина", address: "ул. Металлистов, 8", phone: "0997792763", latitude: 50.4491824, longitude: 30.4480464)
    
    var array: [Teammate]
    
    init() {
        array = [teammate1, teammate2, teammate3, teammate4, teammate5]
    }
}
